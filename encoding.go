package main

import (
	"bufio"
	"io"

	"golang.org/x/text/encoding/charmap"
)

func readWin1252(rd io.Reader) io.Reader {
	return charmap.Windows1252.NewDecoder().Reader(rd)
}

func writeWin1252(wr io.Writer) io.Writer {
	return charmap.Windows1252.NewEncoder().Writer(wr)
}

func scanWin1252(rd io.Reader) *bufio.Scanner {
	r := readWin1252(rd)
	return bufio.NewScanner(r)
}

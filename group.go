package main

import (
	"fmt"
	"log"
	"path/filepath"
)

type group struct {
	Directory         string
	Projects          []*project
	MapDllBaseAddress map[string]string
}

func (gr *group) loadProjects(preserveBinComp map[string]bool, dolog bool) error {
	for _, p := range gr.Projects {
		err := p.loadVBPBasics(dolog)
		if err != nil {
			return fmt.Errorf("loading basic properties: %v", err)
		}
		p.PreserveBinComp = preserveBinComp[p.Name]
	}

	// we need a 2nd pass for resolving local dependencies
	for _, p := range gr.Projects {
		err := p.loadInternalDependencies(gr)
		if err != nil {
			return fmt.Errorf("loading dependencies: %v", err)
		}
	}

	return nil
}

// hasTarget is true if the dependency d within p
// references a project of the group
func (gr *group) hasTarget(p *project, d *dependency) bool {
	path := filepath.Join(p.FullDirectory, d.RelativePathToArtifact)
	for _, p := range gr.Projects {
		if p.artifactPath() == path {
			return true
		}
	}
	return false
}

func (gr *group) project(d *dependency) (*project, bool) {
	for _, p := range gr.Projects {
		// log.Println("group.project seeks d.RelativeUnixDirectory:", d.RelativeUnixDirectory,"in p.RelativeUnixDirectory:", p.RelativeUnixDirectory)
		if p.RelativeUnixDirectory == d.RelativeUnixDirectory {
			return p, true
		}
	}
	return nil, false
}

func (gr *group) find1stBuild() (*project, error) {
	foundNonEmpty := false
	for _, p := range gr.Projects {
		list, err := p.buildsNeededInclSelf(gr)
		if err != nil {
			return nil, err
		}
		switch len(list) {
		case 0:
			// not to build
		case 1:
			return list[0], nil
		default:
			foundNonEmpty = true
		}
	}
	if foundNonEmpty {
		return nil, fmt.Errorf("all projects list either zero or two or more builds, that is impossible")
	}
	return nil, nil // all done
}

func (gr *group) logTotals() {
	pr, countSrc, tloc := 0, 0, 0
	for _, p := range gr.Projects {
		pr++
		countSrc += p.CountSources
		tloc += p.TotalLOC
	}
	log.Printf("projects %d, source files %d, total lines %d", pr, countSrc, tloc)
}

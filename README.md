# vb6bcbuild - VB6 Binary Compatible BUILDer

This Go program helps to build binary compatible
VB6 components listed in a vb group file, defaulting to
Template.vbg. Components are built only if the SHA
hashes of its source files change. Hashes and dependencies
are stored as JSON in a \*.sha file parallel to the
\*.vbp file to detect builds needed, even though git
doesn't keep track of a file's commit date. With an
additional option the tool also does project compatible
optimized builds like make.

Binary component files are created as copies of
DLLs, EXEs and OCXs and refered to as \*.cmp files.
After a single build, vbp files are put back to
project compatibility mode referring to it's own
binary artifact, unless an exception list declares
a project to keep the binary compatibility mode.

## Status

**beta**/**works on my machine** - all intended features have an implementation and
we go into UAT testing now. However, and of course, we expect several
bug reports and improvement request that will be handled as time goes by.
We don't expect serious data corruption or completely false operation. There
might be some missed sources or dependencies, and probably some extra or missed
builds, all of which can be resolved manually or cost unneccessary waits. Build order
should be OK but we don't have much evidence, yet.

## Intention - There are Two Basic Ideas

Not having actual file dates under git repos
makes it hard for make and friends to optimize VB6 builds.
We replace dependency change detection via file dates
by using sha256 hashes of the sources. Hashes for text sources
ignore casing and each line is trimmed.

Furthermore, VB6 has the concept of building some COM
component in binary compatible mode in strict and advanced
varieties. I.e. when some change does not ("strict") or
only extend ("advanced") the public interface (aka the
binary VTable) of COM classes in a component, it can be
recompiled without needing to rebuild other components
referencing this lib.
The tool automates and optimizes such binary compatible
builds in both modes.

## Points of Interest / Design

To make the program not overly complicated, we assume
these statements to hold:

- all files must be under source control with git or a similar vcs.
  Keep a backup of all files. In case of errors some files can get
  corrupted easyliy. So keep an eye on your commits and backups
  before running this tool
- there are no _projectname_.cmp files in use for project purposes
- there are no _projectname_.sha files in use for project purposes
- a binary companion file, like for frm/frx and ctl/ctx pairs
  (that are not listed in the vbp as sources) is detected by its
  presence on disk. The criteria is to change the last letter of a source
  file's path to an `x`, then load the file if it exists and add its
  contents to the sha hash of the source file
- we use this for a VB6 group with these stats:
  "projects 38, source files 1809, total lines 741272".
  On my hardware, hashing the sources from SSD is parallelized
  with Go routines and takes a second or two. If the files are
  cached by the OS, there is nearly no noticeable delay.
  It may or may not be suitable for larger or smaller vbg groups,
  however
- parallel build opportunities could be detected easily. However,
  managing multiple build processes at the same time is not easy-peasy
  and most probably not worth doing b/c binary compatibility builds are
  most often done for bugfixes, often located to one or few components
  and also depending on each other. Thus we excpect mostly no potential
  for parallelization
- all files on disk are processed with Windows-1252 (Western-Europe) encoding
- source code for the sha256 hashes is trimmed line by line and coverted to
  lower case. I.e. if a change only affects the casing of words within
  string literals, it's not detected as a change. As a workaround, also
  add or change some comment in the file to trigger a recognized change.
  Background: Some "`Dim X, Y`\<enter\>" changes every `Call x.y()`
  to `Call X.Y()` everywhere. As VB6 is case-insensitive, this change is
  irrelevant for the binary. By hashing lower-case the tool ignores such
  casing-only changes. By trimming lines, indentation-only changes won't
  trigger a rebuild, too
- break-up or joins of lines with `_` are recognized as a change even if
  they aren't
- the `-bincomp` csv list uses only project names and not paths. This might get
  hard if a project group uses duplicate names in different paths. Workaround:
  don't use duplicate names with fixed binary compatibility
- reference cycles are forbidden. The program never finishes if such a
  cycle is present. Workaround: don't do circular references. It _may_
  be possible to get a working build with all binary compatibility, but
  I guess that's hard to maintain, anyway
- It's targetting the older version 1.16 of Go, b/c I need
  a 32bit executable on Windows. The program itself is based on the standard
  library and two Go extended library packages (golang.org/x/...)

## Detail Features / TODOs

- [x] loading vbg, vbp and parsing them (with Windows-1252 encoding)
  (esp. special resources and UI references might not work yet)
- [x] loading and hashing source files
- [x] load and hash associated binaries like frx and ctx files
- [x] persist hashes in sha files (including `-reset` mode)
- [x] detecting changes in sources (including `-simulate` mode)
  (we don't need to check dependencies b/c it's only a change if some source code
  file also changes, i.e. we don't care if some _different_ ref has the _same API_)
- [x] recursively detect what to build by inspecting the internal dependency graph
- [x] locating vb6.exe on disk (32bit and 64bit OSes)
- [x] copying artifacts to \*.cmp files
- [x] changing to binary compatibility and to \*.cmp in the vbp file
- [x] starting the compiler
- [x] restoring project compatibility in the vbp file
- [x] exceptions for keeping binary compatibility in the vbp file
- [x] "low hanging fruit" statistics like counts and LOCs
- [x] cross analysis of DllBaseAddress settings, with notes
  (b/c we already parse vbp files of a group)
- [x] transitive inner references analysis, with notes
- [x] project compatibility builds, i.e. like make but with hashes instead of file dates
- [x] some real live testing - a single bugfix compile works on a 32bit OS
- [ ] test loading and parsing vbps
- [ ] more real live testing
- [ ] call a batch or a program after compilation, e.g. to digitally sign the produced binary (for v0.9)

## On Binary Compatibility and Build Ordering

If we only have _strict_ binary compatibility,
i.e. the VTable doesn't change at all,
build ordering is irrelvant. This is of limited use.

Normally we use _advanced_ binary compatibility,
i.e. the VTable is identical in the historic part, but
it can be tail-extended to allow for new public methods
on a class. If that happens there is some new dependent
source code that uses the new methods and probably in
between there is some binary that passes pointers to this
enlarged classes with unchanges sources. The build order must
then ensure that the enlarged class artifact is built first
and then the client using the new methods, it won't compile
otherwise. The artifact in the middle, however, can be kept
as is b/c of the guarantees advanced binary compatibility
gives.

We therefore honor dependencies in a make-like way and always
do topologic sorting to get the build order correct.

## Usage

To use the program, head to the vb repo containing a VB group file
named `template.vbg`.

### Build the Tool from Sources

`go install gitlab.com/lercher/vb6bcbuild@latest`

### Record Baseline Situation

Only once you need to record the current baseline of your group. To do so:

1. build the VB program group manually to have all the binaries on disk
1. run `vb6bcbuild -reset`
1. binaries and sha files match with each other
1. it writes `*.sha` files (LF terminated) you need to add to git: `git add *.sha`
   and commit them

### After Some Change to the Source Files of the VB Group

1. commit your changes
1. run `vb6bcbuild` and watch the show. Note: VB6 displays no UI nor
   command line output during compiling
1. commit the changed `*.sha` files to create and conserve the new baseline
1. deliver and repeat

### Keeping an Eye on Broken Compatibility

You need to resolve this manually by
by following the steps of the section "Record Baseline Situation",
i.e. manually build the group which was left in project compatibility mode
and then run `vb6bcbuild -reset`.

Alternatively you can run `vb6bcbuild -projcomp` to start a make-like
project compatibe build which also compiles every dependent component
of a source-code-changed project.

### Customize

1. run `vb6bcbuild -?` to see the current command line options
1. consider conserving binary artifacts and `*.cmp` files in the git repo
   or somewhere else for future reference. I guess, if you know of the term binary
   compatibility in VB6, there is some concept to preserve these compatibility
   files anyway
1. there are currently no options to customise the tool's extensions `.cmp` and `.sha`
   and there are no plans to support such a change. May the fork be with you, technically
   it's not that hard to implement

## Example Program Output (no VB6 installed)

```log
C:\git\src\gitlab.com\lercher\vb6bcbuild>go build && .\vb6bcbuild -dir \git\src\innersource.soprasteria.com\leasysoft\leasy -vbg lsservice.vbg
2023/07/14 22:27:27 This is vb6bcbuild, (C) 2023 by Martin Lercher
2023/07/14 22:27:27 see https://gitlab.com/lercher/vb6bcbuild for details
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H**000000 missing in lsLS\lsAITrans\lsAITrans.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H**000000 missing in lsLS\lsAIVerarbeitung\lsAIVerarbeitung.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H20000000 setting of lsLS\lsAIRI\lsAIRI.vbp conflicts with lsLS\lsAIMI\lsAIMI.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H21000000 setting of lsLS\lsAITask\lsAITask.vbp conflicts with lsLS\lsAIAuskunft\lsAIAuskunft.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H**000000 missing in lsLS\lsAIMietAnp\lsAIMietAnp.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H**000000 missing in lsLS\lsAIWechsel\lsAIWechsel.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H**000000 missing in lsTPS\lsTPS.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H23000000 setting of lsFIBU\NullFIBUAdapter\NullFibuAdapter.vbp conflicts with lsLS\lsAIAI\lsAIAI.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H21000000 setting of lsFIBU\lsAISollstellung\lsAISollstellung.vbp conflicts with lsLS\lsAIAuskunft\lsAIAuskunft.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H21000000 setting of lsFIBU\lsAIDtaus\lsAIDtaus.vbp conflicts with lsLS\lsAIAuskunft\lsAIAuskunft.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H23000000 setting of lsFIBU\XMLFIBUAdapter\XMLFibuAdapter.vbp conflicts with lsLS\lsAIAI\lsAIAI.vbp
2023/07/14 22:27:27 WARNING: DllBaseAddress=&H23000000 setting of lsFIBU\eGeckoFibuAdapter\eGeckoFibuAdapter.vbp conflicts with lsLS\lsAIAI\lsAIAI.vbp
2023/07/14 22:27:27 Note: missing or overlapping DllBaseAddress settings within one process
2023/07/14 22:27:27   may cause higher program load times because a lot of method addresses
2023/07/14 22:27:27   need to be rewritten to unique addresses by the program loader
2023/07/14 22:27:27 projects 38, source files 1809, total lines 741329
2023/07/14 22:27:27 ERROR
2023/07/14 22:27:27   vb6.exe not found at (programfiles)\Microsoft Visual Studio\VB98\Vb6.exe
2023/07/14 22:27:27   compilation step will just open notepad in the following
2023/07/14 22:27:27   if some project needs to be compiled
lsStore - copy lsStore.dll to *.cmp file
lsStore - rewrite vbp file to binary compatibility
lsStore - compiling 216 source files
lsStore - rewrite vbp file to project compatibility
lsStore - save sha files

lsLS - copy lsLS.dll to *.cmp file
lsLS - rewrite vbp file to binary compatibility
lsLS - compiling 194 source files
lsLS - rewrite vbp file to project compatibility
lsLS - save sha files

lsAITrans - copy lsAITrans.ocx to *.cmp file
lsAITrans - rewrite vbp file to binary compatibility
lsAITrans - compiling  74 source files
lsAITrans - rewrite vbp file to project compatibility
lsAITrans - save sha files

2023/07/14 22:29:01 DONE - elapsed 1m34.1634917s
```

### Simulate

```log
C:\git\src\gitlab.com\lercher\vb6bcbuild>go build   && .\vb6bcbuild -dir \git\src\innersource.soprasteria.com\leasysoft\leasy -vbg lsservice.vbg -simulate
2023/07/15 14:40:38 This is vb6bcbuild v0.7a, (C) 2023 by Martin Lercher
2023/07/15 14:40:38 see https://gitlab.com/lercher/vb6bcbuild for details
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H**000000 missing in lsLS\lsAITrans\lsAITrans.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H**000000 missing in lsLS\lsAIVerarbeitung\lsAIVerarbeitung.vbp
2023/07/15 14:40:38 lsAIRI          ucFZKonditionsauswahl.ctl      current 515bb79e != historic 370ca49a
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H20000000 setting of lsLS\lsAIRI\lsAIRI.vbp conflicts with lsLS\lsAIMI\lsAIMI.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H21000000 setting of lsLS\lsAITask\lsAITask.vbp conflicts with lsLS\lsAIAuskunft\lsAIAuskunft.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H**000000 missing in lsLS\lsAIMietAnp\lsAIMietAnp.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H**000000 missing in lsLS\lsAIWechsel\lsAIWechsel.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H**000000 missing in lsTPS\lsTPS.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H23000000 setting of lsFIBU\NullFIBUAdapter\NullFibuAdapter.vbp conflicts with lsLS\lsAIAI\lsAIAI.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H21000000 setting of lsFIBU\lsAISollstellung\lsAISollstellung.vbp conflicts with lsLS\lsAIAuskunft\lsAIAuskunft.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H21000000 setting of lsFIBU\lsAIDtaus\lsAIDtaus.vbp conflicts with lsLS\lsAIAuskunft\lsAIAuskunft.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H23000000 setting of lsFIBU\XMLFIBUAdapter\XMLFibuAdapter.vbp conflicts with lsLS\lsAIAI\lsAIAI.vbp
2023/07/15 14:40:38 WARNING: DllBaseAddress=&H23000000 setting of lsFIBU\eGeckoFibuAdapter\eGeckoFibuAdapter.vbp conflicts with lsLS\lsAIAI\lsAIAI.vbp
2023/07/15 14:40:38 Note: missing or overlapping DllBaseAddress settings within one process
2023/07/15 14:40:38   may cause higher program load times because a lot of method addresses
2023/07/15 14:40:38   need to be rewritten to unique addresses by the program loader
2023/07/15 14:40:38 projects 38, source files 1809, total lines 741329
2023/07/15 14:40:38 ERROR
2023/07/15 14:40:38   vb6.exe not found at (programfiles)\Microsoft Visual Studio\VB98\Vb6.exe
2023/07/15 14:40:38   compilation step will just open notepad in the following
2023/07/15 14:40:38   if some project needs to be compiled
2023/07/15 14:40:38
2023/07/15 14:40:38 lsAIRI - simulate: compile  18 source files
2023/07/15 14:40:38
2023/07/15 14:40:38 DONE - elapsed 220.5793ms
```

## Command Line Options (v0.8)

```log
...> vb6bcbuild.exe -?
2023/07/15 16:33:33 This is vb6bcbuild v0.8, (C) 2023 by Martin Lercher
2023/07/15 16:33:33 see https://gitlab.com/lercher/vb6bcbuild for details
flag provided but not defined: -?
Usage of vb6bcbuild.exe:
  -bincomp string
        comma separated list of project names for which not to change the vbp file back to project compatibility after a build, i.e. it stays binary compatible (default "lsDienst")
  -dir string
        find the VB6 project group in this directory (default ".")
  -projcomp
        build project compatible and make-like
  -reset
        just compute hashes and dependencies and write them to *.sha files
  -simulate
        detect and print the results, if nothing is printed all is current
  -vbg string
        VB6 group file (default "Template.vbg")
  ```

package main

import (
	"path/filepath"
	"strings"
)

// dependency recognizes only references to
// projects which are also listed in the VBG file.
// We call that "internal dependency"
type dependency struct {
	Description                string   `json:"-"`
	RelativeUnixDirectory      string   `json:"-"`
	RelativeUnixPathToArtifact string   `json:"path,omitempty"`
	RelativePathToArtifact     string   `json:"-"`
	Version                    string   `json:"version,omitempty"`
	Project                    *project `json:"-"`
}

// internalDependencyOf creates a dependency if it is a "Reference="" line
// with 5 components and returns true if it also appears in the group
func internalDependencyOf(gr *group, p *project, line string) (*dependency, bool) {
	if !strings.HasPrefix(line, "Reference=") {
		return nil, false
	}
	// lsLS\lsAIRI\lsAIRI.vbp
	// 0                                                   1     2 3                        4
	// Reference=*\G{D3CAC0D2-F8A2-11D2-AE7A-00104BBC16FA}#3fb.0#0#..\..\lsStore\lsStore.dll#LeasySOFT.pro Storage Engine
	// lsLS\lsAIRI +  ..\..\lsStore\lsStore.dll -> lsStore\lsStore.dll
	parts := strings.Split(line, "#")
	if len(parts) != 5 {
		return nil, false
	}

	d := &dependency{}
	d.Version = parts[1]
	d.RelativePathToArtifact = parts[3]
	d.RelativeUnixPathToArtifact = filepath.ToSlash(filepath.Join(p.RelativeDirectory, d.RelativePathToArtifact))
	d.RelativeUnixDirectory, _ = filepath.Split(d.RelativeUnixPathToArtifact)
	d.Description = parts[4]

	return d, gr.hasTarget(p, d)
}

/*
0                                                   1   2 3                               4
Reference=*\G{00020430-0000-0000-C000-000000000046}#2.0#0#C:\Windows\SysWOW64\stdole2.tlb#OLE Automation
Reference=*\G{D3CABFF4-F8A2-11D2-AE7A-00104BBC16FA}#5d.12#0#..\lsDienst\lsDienst.dll#LeasySOFT.pro Vertikale Dienste
Reference=*\G{420B2830-E718-11CF-893D-00A0C9054228}#1.0#0#C:\Windows\SysWOW64\scrrun.dll#Microsoft Scripting Runtime
Reference=*\G{3F4DACA7-160D-11D2-A8E9-00104B365C9F}#1.0#0#C:\Windows\SysWOW64\vbscript.dll\2#Microsoft VBScript Regular Expressions
Reference=*\G{0E59F1D2-1FBE-11D0-8FF2-00A0D10038BC}#1.0#0#C:\Windows\SysWOW64\msscript.ocx#Microsoft Script Control 1.0
Reference=*\G{F5078F18-C551-11D3-89B9-0000F81FE221}#6.0#0#C:\Windows\SysWOW64\msxml6.dll#Microsoft XML, v6.0
Reference=*\G{0C0FF45D-87C8-4333-9075-3D9B4D64F9FC}#6.0#0#..\lsLS\Share\msado60_Backcompat_i386.tlb#Microsoft ActiveX Data Objects 6.0 BackCompat Library
*/

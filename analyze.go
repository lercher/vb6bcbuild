package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type analyze struct {
	Directory     string
	Template      string
	FullPath      string
	Reset         bool
	Simulate      bool
	BuildProjComp bool
	PreserveBC    string
	VB6exe        string
}

const vb6suffix = `\Microsoft Visual Studio\VB98\Vb6.exe`

func locateVB6() string {
	// C:\Program Files (x86)\Microsoft Visual Studio\VB98\Vb6.exe
	// %ProgramFiles(x86)%\Microsoft Visual Studio\VB98\Vb6.exe on 64bit OS
	// or
	// %ProgramFiles%\Microsoft Visual Studio\VB98\Vb6.exe on 32bit OS

	if pfenv := os.Getenv("ProgramFiles(x86)"); pfenv != "" {
		p := filepath.Join(pfenv, vb6suffix)
		stat, err := os.Stat(p)
		if err == nil && !stat.IsDir() {
			return p
		}
	}
	if pfenv := os.Getenv("ProgramFiles"); pfenv != "" {
		p := filepath.Join(pfenv, vb6suffix)
		stat, err := os.Stat(p)
		if err == nil && !stat.IsDir() {
			return p
		}
	}
	return ""
}

func (a *analyze) run() error {
	a.FullPath = filepath.Join(a.Directory, a.Template)
	f, err := os.Open(a.FullPath)
	if err != nil {
		return err
	}
	defer f.Close()

	gr, err := a.loadProjectGroup(f)
	if err != nil {
		return fmt.Errorf("%v: %v", a.FullPath, err)
	}

	dolog := a.Simulate
	err = gr.loadProjects(a.preserveBCMap(), dolog)
	if err != nil {
		return fmt.Errorf("%v: %v", a.FullPath, err)
	}

	a.dllBaseAddrAnalysis(gr)

	err = a.transitiveRefAnalysis(gr)
	if err != nil {
		return err
	}

	gr.logTotals()

	switch {
	case a.BuildProjComp:
		// traditional make process
		a.locateVB6AndLog()

		for _, p := range gr.Projects {
			if p.NeedsBuild {
				log.Printf("%v - needs build because source code changed", p.RelativePathToVBP)
			}
		}

		complete := false
		for !complete {
			complete = true
			// we repeat scanning direct dependencies until we
			// didn't flag a new project to be built
			// this way we also get hold of transitive dependencies
		projectLoop:
			for _, p := range gr.Projects {
				for _, d := range p.Dependencies {
					if !p.NeedsBuild {
						if dp, ok := gr.project(d); ok {
							if dp.NeedsBuild {
								complete = false // do another loop over all projects
								p.NeedsBuild = true
								log.Printf("%v - needs build because direct dependency %v needs build", p.RelativePathToVBP, dp.RelativePathToVBP)
								continue projectLoop
							}
						}
					}
				}
			}
		}

		// we now marked all projects that need a build either b/c
		// of source code change or b/c a dependency needs to be built
		for {
			log.Println()

			p, err := gr.find1stBuild()
			if err != nil {
				return fmt.Errorf("find1stBuild: %v", err)
			}
			if p == nil {
				break
			}

			if a.Simulate {
				log.Printf("%v - simulate: compile %3d source files", p.Name, len(p.Sources))
				p.NeedsBuild = false
				// and start over
				continue
			}

			//  make vbp proj comp +++ compile dll +++ copy dll->cmp +++ save sha

			if p.PreserveBinComp {
				log.Printf("%v - keeping vbp file as binary compatibible", p.Name)
			} else {
				log.Printf("%v - rewrite vbp file to project compatibility", p.Name)
				err = p.rewriteVBP("dll") // anything else than "cmp" will restore the exe32 name
				if err != nil {
					return fmt.Errorf("rewrite project compatibility: %v", err)
				}
			}

			log.Printf("%v - compiling %3d source files", p.Name, len(p.Sources))
			var cmd *exec.Cmd
			if a.VB6exe == "" {
				cmd = exec.Command("notepad.exe", p.FullPathToVBP)
			} else {
				cmd = exec.Command(a.VB6exe, p.FullPathToVBP, `/make`)
			}
			err = cmd.Run()
			if err != nil {
				return fmt.Errorf("vb6 compiler error: %v", err)
			}

			log.Printf("%v - copy %v to *.cmp file", p.Name, p.ExeName32)
			err = p.copyArtifact()
			if err != nil {
				return fmt.Errorf("copy binary: %v", err)
			}

			log.Printf("%v - save sha file", p.Name)
			err = p.saveSHA()
			if err != nil {
				return err
			}

			p.NeedsBuild = false
			// and start over
		}

	default:
		// make in binary compatible mode
		a.locateVB6AndLog()

		for {
			log.Println()

			p, err := gr.find1stBuild()
			if err != nil {
				return fmt.Errorf("find1stBuild: %v", err)
			}
			if p == nil {
				break
			}

			if a.Simulate {
				log.Printf("%v - simulate: compile %3d source files", p.Name, len(p.Sources))
				p.NeedsBuild = false
				// and start over
				continue
			}

			// copy dll->cmp +++ make vbp bin comp +++ compile dll +++ make vbp proj comp +++ save sha

			log.Printf("%v - copy %v to *.cmp file", p.Name, p.ExeName32)
			err = p.copyArtifact()
			if err != nil {
				return fmt.Errorf("copy binary: %v", err)
			}

			log.Printf("%v - rewrite vbp file to binary compatibility", p.Name)
			err = p.rewriteVBP("cmp")
			if err != nil {
				return fmt.Errorf("rewrite binary compatibility: %v", err)
			}

			log.Printf("%v - compiling %3d source files", p.Name, len(p.Sources))
			var cmd *exec.Cmd
			if a.VB6exe == "" {
				cmd = exec.Command("notepad.exe", p.FullPathToVBP)
			} else {
				cmd = exec.Command(a.VB6exe, p.FullPathToVBP, `/make`)
			}
			err = cmd.Run()
			if err != nil {
				return fmt.Errorf("vb6 compiler error: %v", err)
			}

			if p.PreserveBinComp {
				log.Printf("%v - keeping vbp file as binary compatibible", p.Name)
			} else {
				log.Printf("%v - rewrite vbp file to project compatibility", p.Name)
				err = p.rewriteVBP("dll") // anything else than "cmp" will restore the exe32 name
				if err != nil {
					return fmt.Errorf("rewrite project compatibility: %v", err)
				}
			}

			log.Printf("%v - save sha file", p.Name)
			err = p.saveSHA()
			if err != nil {
				return err
			}

			p.NeedsBuild = false
			// and start over
		}

	case a.Reset:
		// just overwrite sha files
		for _, p := range gr.Projects {
			err = p.saveSHA()
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (a *analyze) locateVB6AndLog() {
	a.VB6exe = locateVB6()
	if a.VB6exe == "" && !a.Simulate {
		log.Println("ERROR")
		log.Println("  vb6.exe not found at (programfiles)" + vb6suffix)
		log.Println("  compilation step will just open notepad in the following")
		log.Println("  if some project needs to be compiled")
	}
}

func (*analyze) dllBaseAddrAnalysis(gr *group) {
	log.Println("DllBaseAddress analysis")
	baseAdrWarning := false
	for _, p := range gr.Projects {
		projPath, ok := gr.MapDllBaseAddress[p.DllBaseAddress]

		switch {
		case strings.HasSuffix(p.ExeName32, ".exe"):
			// DllBaseAddress=&H##000000 not needed

		case p.DllBaseAddress == "":
			log.Println("WARNING: DllBaseAddress=&H**000000 missing in", p.RelativePathToVBP)
			baseAdrWarning = true

		case ok:
			log.Println("WARNING: DllBaseAddress="+p.DllBaseAddress, "setting of", p.RelativePathToVBP, "conflicts with", projPath)
			baseAdrWarning = true

		default:
			gr.MapDllBaseAddress[p.DllBaseAddress] = p.RelativePathToVBP
		}
	}
	if baseAdrWarning {
		log.Println("Note: missing or overlapping DllBaseAddress settings within one process")
		log.Println("  may cause higher program load times because a lot of method addresses")
		log.Println("  need to be rewritten to unique addresses by the program loader")
	}
}

func (*analyze) transitiveRefAnalysis(gr *group) error {
	log.Println("transitive inner references analysis")
	transitiveWarning := false
	for _, p := range gr.Projects {
		trans, err := p.transitveDependencies(gr)
		if err != nil {
			return fmt.Errorf("transitive reference analysis: %v", err)
		}
		for _, tp := range trans {
			log.Printf("WARNING: %v transitively uses %v - it should be a direct reference", p.RelativePathToVBP, tp.RelativePathToVBP)
			transitiveWarning = true
		}
	}
	if transitiveWarning {
		log.Println("Notes: referenced components usually need and return classes and methods")
		log.Println("  of their dependencies. To use them properly in a project it needs a")
		log.Println("  direct reference to this transitively used third component. It's")
		log.Println("  recommended to add such a reference. We also have reports of failing")
		log.Println("  compiler processes after compiling and closing the vb6 IDE. This happens")
		log.Println("  regardless of vb6 started in GUI or headless mode.")
		log.Println("  Transitive reference analysis only inspects formal references saved in")
		log.Println("  vbp files, it doesn't detect if source code actually uses these components.")
	}
	return nil
}

func (a *analyze) preserveBCMap() map[string]bool {
	list := strings.Split(a.PreserveBC, ",")
	m := make(map[string]bool)
	for i := range list {
		m[strings.TrimSpace(list[i])] = true
	}
	return m
}

func (a *analyze) loadProjectGroup(r io.Reader) (*group, error) {
	gr := &group{
		Directory:         a.Directory,
		MapDllBaseAddress: make(map[string]string),
	}

	sc := scanWin1252(r)
	if sc.Scan() {
		if sc.Text() != "VBGROUP 5.0" {
			return nil, fmt.Errorf("not a vb6 project group file")
		}
	}
	for sc.Scan() {
		line := strings.TrimSpace(sc.Text())
		line = strings.TrimPrefix(line, "StartupProject=")
		line = strings.TrimPrefix(line, "Project=")
		if line != "" {
			p := &project{}
			p.RelativePathToVBP = line
			p.RelativeUnixPathToVBP = filepath.ToSlash(p.RelativePathToVBP)
			p.RelativeDirectory, _ = filepath.Split(p.RelativePathToVBP)
			p.RelativeUnixDirectory = filepath.ToSlash(p.RelativeDirectory)
			p.FullPathToVBP = filepath.Join(a.Directory, p.RelativePathToVBP)
			p.FullPathToSHA = strings.TrimSuffix(p.FullPathToVBP, filepath.Ext(p.FullPathToVBP)) + ".sha"
			p.FullDirectory, p.Name = filepath.Split(p.FullPathToVBP)
			p.Name = strings.TrimSuffix(p.Name, filepath.Ext(p.Name))

			gr.Projects = append(gr.Projects, p)
		}
	}

	return gr, nil
}

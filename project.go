package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/sync/errgroup"
)

type project struct {
	Name                  string        `json:"name,omitempty"`
	ExeName32             string        `json:"exeName32,omitempty"`                   // from ExeName32="some.dll"
	PreserveBinComp       bool          `json:"preserveBinaryCompatibility,omitempty"` // don't return to project compatibility
	FullDirectory         string        `json:"-"`
	FullPathToVBP         string        `json:"-"`
	FullPathToSHA         string        `json:"-"`
	RelativeDirectory     string        `json:"-"`
	RelativeUnixDirectory string        `json:"-"`
	RelativePathToVBP     string        `json:"-"`
	RelativeUnixPathToVBP string        `json:"path,omitempty"`
	CountSources          int           `json:"sourcesFiles,omitempty"`
	TotalLOC              int           `json:"totalLines,omitempty"`
	Sources               []*source     `json:"sources,omitempty"`
	Dependencies          []*dependency `json:"dependencies,omitempty"`
	Historic              *project      `json:"-"` // can be nil, e.g. if there is no sha file
	NeedsBuild            bool          `json:"-"`
	DllBaseAddress        string
}

// artifactPath combines RelativePath with ExeName32
func (p project) artifactPath() string {
	return filepath.Join(p.FullDirectory, p.ExeName32)
}

func (p project) componentPath() string {
	ap := p.artifactPath()
	return strings.TrimSuffix(ap, filepath.Ext(ap)) + ".cmp"
}

func (p *project) buildsNeededInclSelf(gr *group) (list []*project, err error) {
	if !p.NeedsBuild {
		return nil, nil
	}
	list = append(list, p) // self need build

	m, err := p.allDependencies(gr)
	if err != nil {
		return nil, err
	}
	for _, v := range m {
		if v.NeedsBuild {
			list = append(list, v)
		}
	}
	return list, nil
}

// transitveDependencies are allDependencies minus direct Dependencies.
// Note, that this list should be empty
func (p *project) transitveDependencies(gr *group) (map[string]*project, error) {
	all, err := p.allDependencies(gr)
	if err != nil {
		return nil, err
	}
	for _, d := range p.Dependencies {
		if dp, ok := gr.project(d); ok {
			delete(all, dp.RelativeUnixPathToVBP)
		}
	}
	return all, nil
}

func (p *project) allDependencies(gr *group) (map[string]*project, error) {
	m := make(map[string]*project, len(gr.Projects))
	err := p.collectDependencies(gr, m)
	return m, err
}

func (p *project) collectDependencies(gr *group, m map[string]*project) error {
	for _, d := range p.Dependencies {
		dp, ok := gr.project(d)
		if !ok {
			// this should not happen, b/c Dependencies are only inner dependencies
			return fmt.Errorf("implementation: project %v references %v which is not in the vbg", p.RelativeUnixPathToVBP, d.RelativeUnixPathToArtifact)
		}
		_, ok = m[dp.RelativeUnixPathToVBP]
		if !ok {
			// not yet visited, go deeper
			m[dp.RelativeUnixPathToVBP] = dp
			dp.collectDependencies(gr, m)
		}
	}
	return nil
}

func (p *project) loadVBPBasics(dolog bool) error {
	f, err := os.Open(p.FullPathToVBP)
	if err != nil {
		return err
	}
	defer f.Close()

	eg := new(errgroup.Group)
	sc := scanWin1252(f)
	for sc.Scan() {
		line := sc.Text()

		switch {
		case strings.HasPrefix(line, "ExeName32="):
			p.ExeName32 = strings.Trim(strings.TrimPrefix(line, "ExeName32="), `"`)

		case strings.HasPrefix(line, "DllBaseAddress="):
			p.DllBaseAddress = strings.Trim(strings.TrimPrefix(line, "DllBaseAddress="), `"`)

		case strings.HasPrefix(line, "Class="),
			strings.HasPrefix(line, "UserControl="),
			strings.HasPrefix(line, "Form="),
			strings.HasPrefix(line, "ResFile32="),
			strings.HasPrefix(line, "Designer="),
			strings.HasPrefix(line, "Module="):

			s := createSourceFrom(p, line)
			eg.Go(func() error {
				return s.computeHash(p)
			})

			p.Sources = append(p.Sources, s)
		}
	}
	eg.Go(p.loadSHA)
	err = eg.Wait()
	if err != nil {
		return err
	}

	if p.Historic != nil {
		if p.Historic.differsFrom(p.Sources, true, dolog) || p.differsFrom(p.Historic.Sources, false, dolog) {
			// if dolog note that "|| p.differsFrom()" is not called if its already differnt by the first call "p.Historic.differsFrom()"
			// so no output from the 2nd pass
			p.NeedsBuild = true
		}
	}

	for _, s := range p.Sources {
		p.CountSources++
		p.TotalLOC += s.LOC
	}

	return nil
}

func (p *project) differsFrom(sources []*source, currentSources, dolog bool) bool {
	// we check both ways, so we only need to check if all p.Sources have the same hashes in sources
	// key is FileSpecUnix b/c this is the only key JSON-serialized
	m := make(map[string]string, len(sources))
	for _, s := range sources {
		m[s.FileSpecUnix] = s.Hash
	}

	different := false
	for _, s := range p.Sources {
		h := m[s.FileSpecUnix] // h is "" if not found which is different from a computed hash
		if h != s.Hash {
			different = true
			if dolog {
				if h == "" {
					h = "--------" // 8
				}
				if currentSources {
					log.Printf("%-15v %-30v current %s != historic %s", p.Name, s.FileSpecUnix, s.Hash[:8], h[:8])
				} else {
					log.Printf("%-15v %-30v current %s != historic %s [2nd pass]", p.Name, s.FileSpecUnix, h[:8], s.Hash[:8])
				}
			} else {
				// if it's already differnt and we don't log, we don't need to continue
				break
			}
		}
	}
	return different
}

func (p *project) loadInternalDependencies(gr *group) error {
	f, err := os.Open(p.FullPathToVBP)
	if err != nil {
		return err
	}
	defer f.Close()

	sc := scanWin1252(f)
	for sc.Scan() {
		line := sc.Text()

		if d, ok := internalDependencyOf(gr, p, line); ok {
			p.Dependencies = append(p.Dependencies, d)
		}
	}

	return nil
}

func (p *project) loadSHA() error {
	f, err := os.Open(p.FullPathToSHA)
	if errors.Is(err, os.ErrNotExist) {
		return nil
	}
	if err != nil {
		return err
	}
	defer f.Close()

	dec := json.NewDecoder(f)
	err = dec.Decode(&p.Historic)
	if err != nil {
		return fmt.Errorf("json decoding %v: %v", p.FullPathToSHA, err)
	}

	return f.Close()
}

func (p *project) saveSHA() error {
	f, err := os.Create(p.FullPathToSHA)
	if err != nil {
		return err
	}
	defer f.Close()

	enc := json.NewEncoder(f)
	enc.SetIndent("", "  ")
	err = enc.Encode(p)
	if err != nil {
		return fmt.Errorf("json encoding %v: %v", p.FullPathToSHA, err)
	}

	return f.Close()
}

func (p *project) copyArtifact() error {
	// we don't stream, binaries should fit into RAM
	bs, err := os.ReadFile(p.artifactPath())
	if err != nil {
		return err
	}
	return os.WriteFile(p.componentPath(), bs, 0666)
}

func (p *project) rewriteVBP(to string) error {
	// CompatibleMode="1" CompatibleEXE32="some.dll"
	// CompatibleMode="2" CompatibleEXE32="some.cmp"

	CompatibleMode, CompatibleEXE32 := "1", p.ExeName32
	if to == "cmp" {
		CompatibleMode = "2"
		CompatibleEXE32 = strings.TrimSuffix(p.ExeName32, filepath.Ext(p.ExeName32)) + ".cmp"
	}

	f, err := os.Open(p.FullPathToVBP)
	if err != nil {
		return err
	}
	defer f.Close()

	buf := new(bytes.Buffer)
	wr := writeWin1252(buf)

	sc := scanWin1252(f)
	for sc.Scan() {
		line := sc.Text()

		switch {
		case strings.HasPrefix(line, "CompatibleMode="):
			_, err = fmt.Fprintf(wr, "CompatibleMode=\"%s\"\r\n", CompatibleMode)

		case strings.HasPrefix(line, "CompatibleEXE32="):
			_, err = fmt.Fprintf(wr, "CompatibleEXE32=\"%s\"\r\n", CompatibleEXE32)

		default:
			_, err = fmt.Fprintf(wr, "%s\r\n", line)
		}
		if err != nil {
			return fmt.Errorf("writing buf %v: %v", p.FullPathToVBP, err)
		}
	}
	err = f.Close()
	if err != nil {
		return fmt.Errorf("closing %v: %v", p.FullPathToVBP, err)
	}

	err = os.WriteFile(p.FullPathToVBP, buf.Bytes(), 0666)
	if err != nil {
		return fmt.Errorf("writing %v: %v", p.FullPathToVBP, err)
	}

	return nil
}

/*
Type=OleDll
Reference=*\G{00020430-0000-0000-C000-000000000046}#2.0#0#C:\Windows\SysWOW64\stdole2.tlb#OLE Automation
Reference=*\G{D3CABFF4-F8A2-11D2-AE7A-00104BBC16FA}#5d.12#0#..\lsDienst\lsDienst.dll#LeasySOFT.pro Vertikale Dienste
Reference=*\G{420B2830-E718-11CF-893D-00A0C9054228}#1.0#0#C:\Windows\SysWOW64\scrrun.dll#Microsoft Scripting Runtime
Reference=*\G{3F4DACA7-160D-11D2-A8E9-00104B365C9F}#1.0#0#C:\Windows\SysWOW64\vbscript.dll\2#Microsoft VBScript Regular Expressions
Reference=*\G{0E59F1D2-1FBE-11D0-8FF2-00A0D10038BC}#1.0#0#C:\Windows\SysWOW64\msscript.ocx#Microsoft Script Control 1.0
Reference=*\G{F5078F18-C551-11D3-89B9-0000F81FE221}#6.0#0#C:\Windows\SysWOW64\msxml6.dll#Microsoft XML, v6.0
Reference=*\G{0C0FF45D-87C8-4333-9075-3D9B4D64F9FC}#6.0#0#..\lsLS\Share\msado60_Backcompat_i386.tlb#Microsoft ActiveX Data Objects 6.0 BackCompat Library
Designer=rptGenericUebersicht.Dsr
Class=CEigenschaften; CEigenschaften.cls
Class=CEigenschaft; CEigenschaft.cls
Class=CEingabe; CEingabe.cls
Class=GClsDT; GClsDT.cls
Class=CSQLObjekte; CSQLObjekte.cls
Module=modGlobal; modGlobal.bas
Module=modHelp; modHelp.bas
UserControl=ucEndabrechnung.ctl
RelatedDoc=Feiertage.xml
Form=frmFeldSuchen.frm
Module=modRERechnung; ..\lsLS\modRERechnung.bas
ResFile32="lsExp.RES"
IconForm="mdiExplorer"
Startup="Sub Main"
HelpFile=""
Title="lsDT"
ExeName32="lsDT.dll"
Command32=""
Name="lsDT"
HelpContextID="0"
Description="Basisdatentypen"
CompatibleMode="1"
CompatibleEXE32="lsDT.dll"
MajorVer=1
MinorVer=0
RevisionVer=1038
AutoIncrementVer=1
ServerSupportFiles=0
DllBaseAddress=&H22000000
VersionCompanyName="Some GmbH"
VersionFileDescription="Datentypen"
VersionLegalCopyright="(c) some GmbH"
VersionProductName="Awesome.pro"
CompilationType=0
OptimizationType=0
FavorPentiumPro(tm)=-1
CodeViewDebugInfo=-1
NoAliasing=0
BoundsCheck=0
OverflowCheck=0
FlPointCheck=0
FDIVCheck=0
UnroundedFP=0
StartMode=1
Unattended=-1
Retained=0
ThreadPerObject=0
MaxNumberOfThreads=1
DebugStartupOption=0

[MS Transaction Server]
AutoRefresh=1
*/

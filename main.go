package main

import (
	"flag"
	"log"
	"time"
)

var (
	flagVBG      = flag.String("vbg", "Template.vbg", "VB6 group file")
	flagDir      = flag.String("dir", ".", "find the VB6 project group in this directory")
	flagBinComp  = flag.String("bincomp", "lsDienst", "comma separated list of project names for which not to change the vbp file back to project compatibility after a build, i.e. it stays binary compatible")
	flagReset    = flag.Bool("reset", false, "just compute hashes and dependencies and write them to *.sha files")
	flagSimulate = flag.Bool("simulate", false, "detect and print the results, if nothing is printed all is current")
	flagProjComp = flag.Bool("projcomp", false, "build project compatible and make-like")
)

func main() {
	log.Println("This is vb6bcbuild v0.8, (C) 2023 by Martin Lercher")
	log.Println("see https://gitlab.com/lercher/vb6bcbuild for details")

	t0 := time.Now()
	flag.Parse()

	a := analyze{
		Template:      *flagVBG,
		Directory:     *flagDir,
		Reset:         *flagReset,
		Simulate:      *flagSimulate,
		PreserveBC:    *flagBinComp,
		BuildProjComp: *flagProjComp,
	}
	err := a.run()

	if err != nil {
		log.Println(err)
		log.Fatalln("FAILED - elapsed", time.Now().Sub(t0))
	}
	log.Println("DONE - elapsed", time.Now().Sub(t0))
}

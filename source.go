package main

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type source struct {
	Type                string `json:"-"`
	Name                string `json:"-"`
	FileSpec            string `json:"-"`
	FileSpecUnix        string `json:"file,omitempty"`
	RelativePathToGroup string `json:"-"`
	HasBinaryCompanion  bool   `json:"hasBinary,omitempty"`
	Hash                string `json:"ci-hash,omitempty"`
	LOC                 int    `json:"lines,omitempty"`
}

// createSourceFrom parses line in vbp and sets Type and paths
func createSourceFrom(p *project, line string) *source {
	parts := strings.SplitN(line, "=", 2)
	s := &source{}
	s.Type = parts[0]
	val := parts[1]

	switch s.Type {
	case "ResFile32":
		// ResFile32="lsExp.RES"
		s.FileSpec = strings.Trim(val, `"`)
		s.RelativePathToGroup = filepath.Join(p.RelativePathToVBP, s.FileSpec)
		_, s.Name = filepath.Split(s.RelativePathToGroup)

	case "UserControl", "Form", "Designer":
		// UserControl=ucEndabrechnung.ctl
		s.FileSpec = val
		s.RelativePathToGroup = filepath.Join(p.RelativePathToVBP, s.FileSpec)
		_, s.Name = filepath.Split(s.RelativePathToGroup)

	default:
		// Module=modRERechnung; ..\lsLS\modRERechnung.bas
		valParts := strings.SplitN(val, "; ", 2)
		if len(valParts) == 2 {
			s.Name = valParts[0]
			s.FileSpec = valParts[1]
			s.RelativePathToGroup = filepath.Join(p.RelativePathToVBP, s.FileSpec)
		} else {
			s.Name = fmt.Sprintf("(%v)", val)
		}
	}
	s.FileSpecUnix = filepath.ToSlash(s.FileSpec)

	return s
}

func (s *source) computeHash(p *project) error {
	path := filepath.Join(p.FullDirectory, s.FileSpec)
	f, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("hashing: %v", err)
	}
	defer f.Close()

	h := sha256.New()
	switch s.Type {
	case "ResFile32":
		_, err = io.Copy(h, f)
		if err != nil {
			return fmt.Errorf("hash-read binary %v: %v", path, err)
		}

	default:
		sc := scanWin1252(f)
		for sc.Scan() {
			line := strings.TrimSpace(sc.Text())
			s.LOC++
			if line != "" {
				line = strings.ToLower(line)
				h.Write([]byte(line))
				h.Write([]byte("\n"))

				if err != nil {
					return fmt.Errorf("hash-read lower trimmed %v: %v", path, err)
				}
			}
		}

		if companion, ok := binaryCompanion(path); ok {
			// log.Println("hashing binary:", companion.Name())
			_, err = io.Copy(h, companion)
			if err != nil {
				return fmt.Errorf("hash-read binary %v: %v", companion.Name(), err)
			}
			s.HasBinaryCompanion = true
		}
	}
	bs := h.Sum(nil)
	s.Hash = fmt.Sprintf("%x", bs)
	return nil
}

func binaryCompanion(path string) (*os.File, bool) {
	companion := path[:len(path)-1] + "x"
	f, err := os.Open(companion)
	if errors.Is(err, os.ErrNotExist) {
		return nil, false
	}
	if err != nil {
		log.Println("binary companion file:", err)
		return nil, false
	}
	return f, true
}

/*
Class=CSQLObjekte; CSQLObjekte.cls
Module=modGlobal; modGlobal.bas
Module=modHelp; modHelp.bas
UserControl=ucEndabrechnung.ctl
RelatedDoc=Feiertage.xml
Form=frmFeldSuchen.frm
Module=modRERechnung; ..\lsLS\modRERechnung.bas
ResFile32="lsExp.RES"
*/

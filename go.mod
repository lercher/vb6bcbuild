module gitlab.com/lercher/vb6bcbuild

go 1.16

require (
	golang.org/x/sync v0.1.0
	golang.org/x/text v0.11.0
)
